from PIL import Image
import sys

images = sys.argv[2:]

for png in images:
   
    pngfig=png.replace(".png","")
    print(pngfig)
    image = Image.open(png)
    size = image.size
    print("image size: ", size)

    xrsize = size[0]*0.25
    yrsize = size[1]*0.25
    print("image resized: ",xrsize,yrsize)
        
    image = image.resize((int(xrsize),int(yrsize)),Image.ANTIALIAS)
    path_to_save = "thumb_"+pngfig+".png"
    print(path_to_save)
    image.save(path_to_save,optimize=True,quality=95)
