# HEPData

Two simple python scripts to help make HEPData submissions

## Scripts

The `make_yaml.py` is a python function script that can be added to your plotting scripts in order to output yaml files for you histograms.
1D histograms (can be stacked or overlayed) and limit plots are implemented. Do note that errors within the 1D histogram function has not been added yet.

Independant variable sections will need to be edited for your use.

An example use of the functions are:
```
from make_yaml import *

print_yaml_1D_stack(hist_list,hist_list_name,"log (Loss)","fig_01")
```
```

observed_list = []
expected_list = []
sigmas = []

limit_name = ["observed limit [pb]","Expected limit [pb]"]

observed_list.append(observed)
expected_list.append(expected1sigma)
sigmas.append(expected2sigma)

print_yaml_limit(observed_list,expected_list,sigmas,limit_name,"limit_figure_name")
```
The `compress_png.py` is a simple script that outputs `thumb_{input}.png`. Default compression is 25%

```
python compress_png.py *.png
```
