# Authored by Jacob Crosby (Oklahoma State University)

import sys
import os
from ROOT import TH1D,TF1,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import ROOT

import yaml

def yaml_dump_variable(name):
  
    yaml_dump = ("""
    - header:
        name: """+ str(name) +""" 
      qualifiers:
      - name: $\sqrt{s}$
        units: TeV
        value: 13
      - name: LUMINOSITY
        units: $fb^{-1}$
        value: 140""")
  
    
    return yaml_dump

def yaml_dump_limit_var(name):
  
    yaml_dump = ("""
    - header:
        name: """ + name + """
        units: pb
      qualifiers:
      - name: $\sqrt{s}$
        units: TeV
        value: 13
      - name: LUMINOSITY
        units: $fb^{-1}$
        value: 140""")
  
    
    return yaml_dump

### ERRORS FOR HISTOGRAMS NOT ADDED
### Arguments: A list of histograms (TH1) to stack, list of their names, X axis title, output figure name
def print_yaml_1D_stack(hists,names,xaxis_title,fig_name):
    num_stack = len(names)
    num_hist = len(hists)
    print("Number of Histograms in Yaml: ",num_hist)
    dump = 'dependent_variables:' 
    total_dump=""
    w=0
    for hist in hists:
        nbins = hist.GetNbinsX()
        bin_value=[]
        for i in range(0,nbins):
           val = hist.GetBinContent(i)
           print(val)
           if val == 0.0 or val == -1.0:
               val = "'-'"
           bin_value.append(val)
        dump = dump+yaml_dump_variable(names[w])
        values = """      values:"""
        value_dump = ""
        for i in range(0,len(bin_value)):
            value_dump = value_dump + os.linesep + "      - value: "+ str(bin_value[i])
        value_dump = values+value_dump
        dump_all = dump+ os.linesep+value_dump
        total_dump = total_dump + dump_all
        dump = ""
        value_dump= ""
        w=w+1
		
    # Now independant variables
    var = 'independent_variables:'
    ind_dump = ("""
- header:
    name: """ + xaxis_title)
    ind_dump = var+ind_dump
    # Get axis
    xaxis = hists[0].GetXaxis()
    bin_up = []
    bin_low = []
    for i in range(0,len(bin_value)):
      bin_up.append(xaxis.GetBinUpEdge(i))
      bin_low.append(xaxis.GetBinLowEdge(i))
    values = """
  values:"""
    bin_dump = ""
    for p in range(0,len(bin_up)):
      bin_dump = bin_dump + os.linesep + "  - high: " + str(bin_up[p])
      bin_dump = bin_dump + os.linesep + "    low: " + str(bin_low[p])
    total_bin_dump = ind_dump + values + bin_dump
    total_dump = total_dump + os.linesep + total_bin_dump
    print(total_dump)
    yaml_dump = yaml.safe_load(total_dump)

    #with open('temp.txt','w') as f:
    #   f.write(total_dump)
    with open (fig_name+'.yaml','w') as file:
      yaml.dump(yaml_dump,file)

### Arguments: A list of TGraphAsymmErrors (observed),  A list of TGraphAsymmErrors (expected),  A list of TGraphAsymmErrors (sigmas) list of names, name of figure
def print_yaml_limit(observed,expected,sigmas,names,xaxis_title,fig_name):
    hist_num = len(observed)
    exp_num = len(expected)
    sigma_num = len(sigmas)
    print("Number of Observed in Limit Yaml: ", hist_num)
    print("Number of Expected in Limit Yaml: ", exp_num)
    print("Number of Sigmas in Limit Yaml: ", sigma_num)
    dump = 'dependent_variables:' 
    total_dump=""
    w=0
    for graph in observed:
        nbins = graph.GetXaxis().GetNbins()
        bin_value=[]
        for i in range(0,nbins):
            val = graph.GetPointY(i)
            if val == 0.0 or val == -1.0:
                val = "'-'"
            bin_value.append(val)
        dump = dump+yaml_dump_limit_var(names[w])
        values = """      values:"""
        value_dump = ""
        for i in range(0,len(bin_value)):
            value_dump = value_dump + os.linesep + "      - value: "+ str(bin_value[i])
        value_dump = values+value_dump
        dump_all = dump+ os.linesep+value_dump
        total_dump = total_dump + dump_all
        dump = ""
        value_dump= ""
        w=w+1
    h=1
    for graph,sigma in zip(expected,sigmas):
      bin_value=[]
      sig1_up = []
      sig1_low = []
      sig2_up = []
      sig2_low = [] 
      for i in range(0,nbins):
          bin_value.append(graph.GetPointY(i))
          sig1_up.append(graph.GetErrorYhigh(i))
          sig1_low.append(graph.GetErrorYlow(i))
          sig2_up.append(sigma.GetErrorYhigh(i))
          sig2_low.append(sigma.GetErrorYlow(i))
      values = yaml_dump_limit_var(names[h])
      values =values + """
      values:"""
      sig_dump = ""
      for i in range(0,nbins):
        values = values + ("""
      - errors:
        - asymerror:
            minus: """ + str(sig1_low[i]) +"""
            plus: """ + str(sig1_up[i]) +"""
          label: 1 s.d.
        - asymerror:
            minus: """ + str(sig2_low[i]) +"""
            plus: """ + str(sig2_up[i]) +"""
          label: 2 s.d.
        value: """ + str(bin_value[i]))
      
      h = h+1
      # Now independant variables
      var = 'independent_variables:'
      ind_dump = ("""
- header:
    name: """ + xaxis_title +"""
    units: TeV
  values:""")
      
    ind_dump = var+ind_dump
   #   steps = 14
   #   step = 0.5
   #   print("steps ", steps) 
   #   value_dump = ""
   #   for i in range(0,steps):
   #     print(i, i*step)
   #     value_dump = value_dump + os.linesep + ("""  - value: """ + str((i+1)*step))     		
    xaxis = observed[0].GetXaxis()
    bin_up = []
    bin_low = []
    for i in range(0,len(bin_value)):
      bin_up.append(xaxis.GetBinUpEdge(i))
      bin_low.append(xaxis.GetBinLowEdge(i))
    values = """
  values:"""
    bin_dump = ""
    for p in range(0,len(bin_up)):
      bin_dump = bin_dump + os.linesep + "  - high: " + str(bin_up[p])
      bin_dump = bin_dump + os.linesep + "    low: " + str(bin_low[p])

    ind_dump = ind_dump+bin_dump
    total_dump = total_dump + os.linesep + ind_dump
    print(total_dump)
    yaml_dump = yaml.safe_load(total_dump)

    with open (fig_name+'.yaml','w') as file:
      yaml.dump(yaml_dump,file)

def TGraph_points_to_yaml(graphs,names,xaxis_title,fig_name):
    hist_num = len(graphs)
    name_num = len(names)
    print("Number of TGraphs in Limit Yaml: ", hist_num)
    print("Number of TGraph names: ", name_num)

    dump = 'dependent_variables:'
    total_dump=""
    nbins = graphs[0].GetXaxis().GetNbins()
    w=0
    for graph in graphs:
        bin_value=[]
        bin_xvalue=[]
        for i in range(0,nbins):
            val = graph.GetPointY(i)
            if val == 0.0 or val == -1.0:
                val = "'-'"
            bin_value.append(val)
            bin_xvalue.append(graph.GetPointX(i))
        dump = dump+yaml_dump_variable(names[w])
        values = """      values:"""
        value_dump = ""
        for i in range(0,len(bin_value)):
            value_dump = value_dump + os.linesep + "      - value: "+ str(bin_xvalue[i])
            value_dump = value_dump + os.linesep + "        value: "+ str(bin_value[i])
        value_dump = values+value_dump
        dump_all = dump+ os.linesep + value_dump
        total_dump = total_dump + dump_all
        dump = ""
        value_dump= ""
        w=w+1
    
    # Now independant variables
    var = 'independent_variables:'
    ind_dump = ("""
- header:
    name: """ + xaxis_title +"""
    units: TeV
  values:""")

    ind_dump = var+ind_dump

    #steps = 14
    #step = 0.5
    #print("steps ", steps)
    #value_dump = ""
    #for i in range(0,steps):
    #  print(i, i*step)
    #  value_dump = value_dump + os.linesep + ("""  - value: """ + str((i+1)*step))
    xaxis = graphs[0].GetXaxis()
    bin_up = []
    bin_low = []
    for i in range(0,len(bin_value)):
      bin_up.append(xaxis.GetBinUpEdge(i))
      bin_low.append(xaxis.GetBinLowEdge(i))
    values = """
  values:"""
    bin_dump = ""
    for p in range(0,len(bin_up)):
      bin_dump = bin_dump + os.linesep + "  - high: " + str(bin_up[p])
      bin_dump = bin_dump + os.linesep + "    low: " + str(bin_low[p])

    ind_dump = ind_dump + bin_dump
    total_dump = total_dump + os.linesep + ind_dump 
    print(total_dump)
    yaml_dump = yaml.safe_load(total_dump)

    with open (fig_name+'.yaml','w') as file:
      yaml.dump(yaml_dump,file) 
